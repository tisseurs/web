---
title: Le Code d'honneur
subtitle: La base éthique qui distingue les tisseurs
comments: false
weight: 30
---

Le but de l'auto-hébergement est de s'évader des Silos à Data corporatistes. Mais ça signifie que la responsabilité sur la protection des données personnelles est transférée au tisseur ou au groupe de tisseurs qui administrent cette ressource. Les compagnies multinationales, fournisseurs de plateformes, prévoient un contrat, sous la forme d'indigestes et interminables 'conditions d'utilisation', EULA, ou autre bullshit, destinés à les couvrir mais remplissant un rôle réel: c'est un contrat, même s'il n'est jamais lu et accepté d'un clic distrait.

Il nous apparaît donc nécessaire, et ça n'a rien de bien original, de spécifier une charte éthique pour les tisseurs. C'est un contrat moral auquel le tisseur digne de ce nom s'engage, et sur lequel les utilisateurs des services maintenus par les tisseurs peuvent compter pour sauvegarder leurs données et leur vie privée. Aucun pouvoir ne devrait être délégué sans un engagement clair envers les responsabilités que ce pouvoir induit.

Nous allons donc dresser une liste de principes moteurs. En termes humains et non légaux, lisibles et simples.

### Les données sont sacrées

Le tisseur est responsable de serveurs, qui hébergent des services et des applications en ligne.

Toute donnée, personnelle ou pas, est confiée aux applications, et par conséquent au tisseur qui les maintient, en délégation. L'utilisateur reste propriétaire de ses données qui ne sont confiées que pour le bon fonctionnement du service

Par conséquent:

- aucun autre usage que celui pour lequel sont fournies ces données ne pourra être toléré
- le tisseur s'engage à tout faire pour protéger les données qui lui sont déléguées contre vols et abus (ainsi que leurs backups).
- le tisseur doit accéder aux demandes des utilisateurs concernant la propriété qu'ils ont de leurs données personnelles, et prévoir un canal clair et connu pour opérer de telles demandes.

### Le logiciel doit être libre

Les tisseurs œuvrent à se ré-approprier la fabrique de l'internet. Mais ce ne peut être fait de façon résiliente que sous réserve de rester ouvert. Ça concerne aussi les solutions logicielles choisies.

Parfois des décisions pragmatiques impliquent pour un groupe l'utilisation de telle ou telle plate-forme propriétaire. Mais le tisseur n'aura de cesse d’œuvrer à la propagation de logiciels libres auto-hébergés.

La culture du libre impacte clairement sur la culture des Tisseurs, l'un rend l'autre possible.

Par conséquent:

- les applications et solutions mises en place par les tisseurs doivent être transparentes et leur code source doit être public.
- le code source utilisé sur les serveurs doit être identique au code publié publiquement (bien évidemment a l'exception des mots de passe et clés de chiffrement s'il y en a).
- autant que possible les ajustement locaux doivent donner lieu a une contribution au logiciel libre modifié

### La transparence est primordiale

L'une des missions les plus critiques du tisseur est d'informer clairement les utilisateurs des évolutions de la plate-forme s'il y a mise a jour, ou s'il dispose soudain de nouveaux services. La visibilité sur le cycle de vie de la plate-forme forge la confiance avec les usagers. Tout ceci force le tisseur à structurer son travail, ce qui le rend plus facile à partager.

Un certain nombre de conventions peuvent être utilisées pour cette structuration, les plus simples étant les meilleures: des readme bien à jour, des changelogs, une newsletter technique si besoin, tous les moyens sont bons. Ce qui compte, c'est que les usagers comprennent ce qu'il se passe et se sentent plus impliqués dans leur environnement numérique.

Par conséquent:

- le tisseur s'engage a informer les utilisateurs des services qu'il héberge, au mieux de ses capacités
- le tisseur autant que possible doit définir les outils et processus de partage de l'information pour que les services ne soient pas un mystère obscur
- le tisseur conserve une approche humble de la collaboration avec ceux qui ne sont pas aguérris aux interfaces numériques

---

> Si vous souhaitez affirmer publiquement votre adhésion au Code d'Honneur des Tisseurs défini ci-dessus, [ajoutez un ficher][ajout] vous décrivant en suivant le modèle d'un tisseur déjà déclaré et soumettez une Merge-Request.
>
> [Liste des Tisseurs déclarant adhérer au Code d'Honneur][liste]

[ajout]: https://git.distrilab.fr/tisseurs/web/src/branch/master/data/tisseurs
[liste]: /tisseurs/