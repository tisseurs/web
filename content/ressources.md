---
title: Ressources de Tissage
subtitle: Une liste de liens utiles au tisseur 
comments: false
weight: 20
---

Cet espace encore un peu vide vise à rassembler une liste classée de liens utiles pour tisseurs de tous niveaux.

En attendant que nous organisions la collecte de ces liens, en voici qui sont d'une évidence manifeste qu'on ne peut pas louper:

### Organisations

- April - https://april.org
- Framasoft - https://framasoft.org
- Chatons - https://chatons.org
- Événements - https://www.agendadulibre.org/tags/auto-h%C3%A9bergement
- Enough - https://enough.community
- Indie Hosters - https://indiehosters.net/
- Collectif point communs - https://point-communs.initiative.place/
- Autistici/Inventati - https://www.autistici.org/

### Media

- Podcast Libre à vous - https://cause-commune.fm/podcastfilter/libre-a-vous/

### Outils

- Guide de l'auto hébergement facile chez FramaSoft - https://framacloud.org/fr/auto-hebergement/
- Yunohost - https://yunohost.org/
- FreedomBox - https://freedomboxfoundation.org/
- Cutting Google out of your life (liste of resources) - https://degoogle.jmoore.dev/
- https://github.com/awesome-selfhosted/awesome-selfhosted
- Devops bookmarks - https://www.devopsbookmarks.org/open-source

### Guides

- Dégooglisons internet - https://degooglisons-internet.org/fr/
- Auto-hébergement.fr - http://www.auto-hebergement.fr/
- Auto-hébergement facile - https://framacloud.org/fr/auto-hebergement/
- Auto-hébergement sur wikipedia - https://fr.wikipedia.org/wiki/Auto-h%C3%A9bergement_(Internet)
- Sortir de Facebook - https://sortirdefacebook.wordpress.com/
- Hygiène numérique sur Android - https://blog.dreads-unlock.fr/ameliorer-son-hygiene-numerique-sur-android/
- Awesome Privacy - https://github.com/pluja/awesome-privacy

### Apprendre

- [Devops roadmap](https://roadmap.sh/devops)
- [Architecture playbook](https://nocomplexity.com/documents/arplaybook/index.html)
- [Making server work (ePub)](https://www.digitalocean.com/community/books/sysadmin-ebook-making-servers-work)

### Social places

- https://www.reddit.com/r/selfhosted/

### Articles et divers

- [13 Outils de de-GAFAMisation](https://numericatous.fr/2020/04/13-outils-de-de-gafamisation/)
- [Monitoring demystified: A guide for logging, tracing, metrics](https://techbeacon.com/enterprise-it/monitoring-demystified-guide-logging-tracing-metrics) (en anglais)
- [Enough with Linux security FUD](https://www.zdnet.com/article/enough-with-the-linux-security-fud/)
- [Free and Public DNS Servers](https://www.lifewire.com/free-and-public-dns-servers-2626062)
