---
title: Trucs Et Astuces
subtitle: Compilation de commandes bien utiles sous linux
comments: false
weight: 30
---

## Partager une console avec tmux
Créer une nouvelle session
```
tmux new-session -s shared
tmux new -s shared
```
(`shared` c'est le nom de la session partagée, qu'on peut remplacer par ce qu'on veut.)

Rejoindre une session existante
```
tmux attach-session -t shared
tmux a -t shared
```

Pour scroller dans tmux : `Ctrl-b` puis `[` 
Ensuite utiliser les touches habituelles pour scroller (flèches haut/bas, PgUp/PgDn). Et tapper `q` pour quitter le mode scrolling.

Créer une autre fenêtre dans une session tmux : `Ctrl-b` puis `c`

Passer d'une fenêtre à l'autre : `Ctrl-b` puis `n` (next) ou `Ctrl-b` puis `p` (previous) ou `Ctrl-b` puis `w` (windows list)

Par défaut les fenêtres prennent le nom du programme en cours d'execution. Mais on peut leur fixer un nom de son choix avec `Ctrl-b` puis `,`

Plus d'info sur tmux : https://www.howtogeek.com/671422/how-to-use-tmux-on-linux-and-why-its-better-than-screen/


## Générer un mot de passe aléatoire
 
mdp de 16 caratères :
```
$ openssl rand -base64 12
hSvo9XgXaMIGX6/0
```
mdp de N caractères, 1<=N<=24 : 
```
$ openssl rand -base64 18 | cut -c -24
4nh6ppEY33szuU++n3wJgAat
```
(remplacer le 24 à la fin pour un mdp plus court)



## PostgreSQL
### psql, le client sql en ligne de commande
Lancer le client en ligne de commande
`sudo -u postgres psql`
`psql -U user -d database -W`
`psql -h host -U user -d database -W`
`psql -h host -U user "dbname=db sslmode=require"`

Puis dans le client psql : 

- Se connecter à une autre base de donnée
`\c db_name`
ou `\c db_name user_name`

- Lister les bases de données du serveur auquel on est connecté
`\l`

- Lister les schemas de la base de donnée courante
`\dn`

- Lister les utilisateurs et leurs rôles
`\du`

- Lister les tables de la db courante
`\dt`

- Lister les colonnes d'une table
`\d table_name` 
ou `\d "Table_Name"` si le nom de la table contient des majuscules

- Rediriger la sortie des requêtes vers un fichier texte
`\o /tmp/file-name.txt`
- Puis pour restorer la sortie par défaut à l'écran
`\o`

- Quitter psql
`\q`


### Autres utilitaires postgresql
Créer un user
`sudo -u postgres createuser -P peertube`

Créer une db
`sudo -u postgres createdb -O owner_user_name -E UTF8 -T template0 db_name`
(le user `owner_user_name` doit avoir été crée au préalable)

Faire un dump d'une db
`sudo -u postgres pg_dump -Fc db_name > /tmp/db_name-dump.db`

Restorer une db a l'identique a partir d'un dump
`sudo -u postgres pg_restore -c -C -d postgres /tmp/peertube_prod-dump.db`
(la db de destination sera suprimée et re-crée avec le nom qu'elle avait au moment du dump)

Restorer un dump dans une db différente de celle d'origine
`sudo -u postgres pg_restore -d other_db_name /tmp/peertube_prod-dump.db`
(la db de destination `other_db_name` doit avoir été crée au préalable)

## vi
https://www.cs.colostate.edu/helpdocs/vi.html

Commandes vi les plus utiles : 

| Commande | Description |
|----------|-------------|
| i | Passer en mode insertion ([Esc] pour en sortir)|
| u | Undo / Redo|
| x | Supprimer un caractère|
| Nx | Supprimer N caractères|
| D | Supprimer la fin de la ligne a partir de la position du curseur|
| dd | Supprimer complètement la ligne courante|
| Ndd ou dNd | Supprimer N lignes|
| yy | Copier (yank) la ligne courante dans le buffer|
| Nyy ou yNy |Copier N lignes dans le buffer|
| p | Coller (paste) le contenu du buffer dansle texte après la ligne courante |


## Rechercher des fichiers

Chercher des fichiers par une partie de leur nom
`find /répertoire/ou/chercher -name '*.jpg'`

La même chose sans distinction de la casse
`find /répertoire/ou/chercher -iname '*.jpg'`

Chercher des fichiers par leur contenu
`find /répertoire/ou/chercher -name '*.txt' -exec grep -li "texte à chercher" {} \;`
L'option -l de grep permet d'afficher les noms des fichiers trouvé plutot que les lignes contenant le texte cherché
L'option -i de grep permet d'ignorer la casse. L'enlever si non nécessaire donne de meilleures performances

La même chose mais dans tous les fichiers, quelque soit leur nom, mais en excluant les répertoires pour éviter que grep ne génère des erreurs
`find /répertoire/ou/chercher -type f -exec grep -li "texte à chercher" {} \;`
(en fait ca exclue plus que les répertoires, les liens symboliques aussi :-( )

## Utiliser une clef SSH pour se connecter au serveur
Afin de se connecter à un serveur, on peut authoriser l'utilisateur d'une machine à se connecter grâce à sa clef SSH publique en rajoutant la clef publique dans le fichier `~/.ssh/authorized_keys` sur le serveur.

Un tuto dispo [ici](https://doc.fedora-fr.org/wiki/SSH_:_Authentification_par_cl%C3%A9)

