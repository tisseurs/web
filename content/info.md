---
title: En bas, il y avait les Tisseurs ...
subtitle: Le petit groupe de Tisseurs à l'origine de ce site
comments: 
weight: 10
---

Nous sommes 12b, MckMonster, Yannick et Mose, et nous nous sommes connus chez les [crapauds fous](https://coa.crapaud-fou.org). Chacun tisseur à sa manière, nous représentons 4 approches techniques différentes mais avec un objectif commun: nous voulons donner le pouvoir aux communautés auxquelles nous participons, sur leurs outils en ligne et sur leurs données. Nous sommes chacun impliqués dans des groupes différents, outre les crapauds fous, où nous jouons un rôle de Tisseur.

Quand Mose a proposé son idée de définir un Code des Tisseurs, il était implicite que notre groupe serait une première instance de groupe de tisseurs (que nous appelons entre nous les Bas-Tisseurs. Ça pète).

Ce site, [hébergé sur ForgeJo][forgejo], est maintenu par ces 4 tisseurs. N'hésitez pas à ouvrir une Merge Request ou une [discussion][issue] sur Gitlab si le cœur vous en dit.

[forgejo]: https://git.distrilab.fr/tisseurs/web
[issue]: https://git.distrilab.fr/tisseurs/web/issues